# Write a program that calculates the Body Mass Index (BMI) from a user's weight and height.

height = float(input("Inform your height: "))
weight = float(input("Inform your weight: "))

bmi = weight / (height ** 2)
bmi_final = "{:.2f}".format(bmi)


print(f"Your bmi is {bmi_final}")